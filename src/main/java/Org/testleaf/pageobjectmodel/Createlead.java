package Org.testleaf.pageobjectmodel;

import com.autoBot.testng.api.base.Annotations;

public class Createlead extends Annotations {



public  Createlead() {}
	
public Createlead entercompanyname() {
	clearAndType(locateElement("id", "companyname"), "testleaf");
	return this;
	
}

public Createlead enterfirstname() {
	clearAndType(locateElement("id", "firstname"), "seenu");
	return this;
}

public Createlead enterlastname() {
	clearAndType(locateElement("id", "lastname"), "s");
	return this;
}

public viewleadpage clickcreatelead() {
	click(locateElement("class","smallSubmit"));
	return  new viewleadpage();
}
	
}
