package Org.testleaf.pageobjectmodel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class Loginpage extends Annotations {



public  Loginpage() {
	PageFactory.initElements(driver, this);
}
    @CacheLookup
	@FindBy(how=How.ID, using="username")
	private WebElement eleusername;
	@FindBy(how=How.ID, using="password")
	private WebElement elepassword;
	@FindBy(how=How.CLASS_NAME, using="decorativesubmit")
	private WebElement elesubmit;

public Loginpage enterusername() {
	clearAndType(eleusername,"DemoSalesManager");
	return this;
}

public Loginpage enterpassword() {
	clearAndType(elepassword, "crmsfa");
	return this;
}

public Homepage clicklogin() {
	click(elesubmit);
	return new Homepage();
}



}