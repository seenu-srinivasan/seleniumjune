package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.HomePage;
import com.autoBot.testng.api.base.Annotations;

import Org.testleaf.pageobjectmodel.Leadpage;
import Org.testleaf.pageobjectmodel.Loginpage;
// Annotations == ProjectBase
public class TC001_LoginAndLogout extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test
	public void loginAndLogout() {
		new Loginpage().enterusername().enterpassword().clicklogin();
		
	}
	
}






